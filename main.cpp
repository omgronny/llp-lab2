#include <iostream>
/*
 * insert:
 * INSERT { firstName: "Anna", name: "Pavlova", profession: "artist" } IN users;
 * INSERT { firstName: "Anna", name: "Pavlova" } IN users;
 * INSERT { firstName: "Anna" } IN users;
 *
 * delete:
 * FOR u IN users FILTER u.status == "deleted" REMOVE u IN users
 *
 * update:
 * FOR u IN users FILTER u.status == "active" UPDATE u WITH { status: "inactive" } IN users
 *
 * select:
 * FOR u IN users FILTER u.status == "active" && u.active == true RETURN u;
 * FOR u IN users FILTER u.age > 18 RETURN u;
 * FOR u IN users FILTER 18 > u.age RETURN u;
 * FOR u IN users RETURN u;
 *
 * join:
 * FOR u IN users
   FOR f IN friends
    FILTER u.id == f.userId
    RETURN u, f
 *
 * my join:
 * FOR u IN users FILTER u.status == "not active" RETURN u JOIN FOR f IN LOSERS RETURN f
 *
 * create:
 * CREATE users { ID: INT, active: STR, else: BOOL }
 *
 * drop:
 * DELETE users
 *
 */

int main() {
    std::cout << "test" << std::endl;
}

