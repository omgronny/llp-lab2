**Глинских Роман P33102**

# Низкоуровневое Программирование. Лабораторная работа 2

## Installation and start

### Linux:

##### Requirements:
- cmake (3.21)
- make
- C++ compiler (Clang 14 or newer, gcc 10 or newer)
- Bison, Flex

```bash
git clone ssh://git@gitlab.se.ifmo.ru:4815/omgronny/llp-lab2.git
cd llp-lab2
make all
```

### Windows:

- cmake (3.21)
- make
- GCC 12.2.0 + MinGW-w64 10.0.0 (you can download it from [here](https://winlibs.com/))
- Bison, Flex

```bash
git clone ssh://git@gitlab.se.ifmo.ru:4815/omgronny/llp-lab2.git
cd llp-lab2
make all
```

## Цель задания
Использовать средство синтаксического анализа по выбору, реализовать модуль для разбора некоторого
достаточного подмножества языка запросов по выбору в соответствии с вариантом формы данных. Должна
быть обеспечена возможность описания команд создания, выборки, модификации и удаления элементов
данных.

## Задачи
1. Изучить выбранное средство синтаксического анализа 
2. Изучить синтаксис языка запросов и записать спецификацию для средства синтаксического анализа
3. Реализовать модуль, использующий средство синтаксического анализа для разбора языка запросов
4. Реализовать тестовую программу для демонстрации работоспособности созданного модуля,
   принимающую на стандартный ввод текст запроса и выводящую на стандартный вывод

## Описание работы и реализации

Структура модуля:
- `tokenizer.l` - Flex файл для генерации токенайзера
- `parser.y` - Bison файл для генерации парсера выражений
- `ast.h, ast.cpp` - определение и реализация структур, представляющих дерево разбора
- `visitor.h, to_string.cpp` - реализация функциональности для вывода элементов Ast в виде дерева

Для разбора выражения использовались *Flex* и *Bison*

Каждый терм в результате разбора запроса выражается как объект
абстрактного класса `Ast`, и содержит в себе какую-то из его реализаций

```c++

struct Ast : std::enable_shared_from_this<Ast> {
    AstType type;
    Ast(AstType type);
    virtual std::string ToString(int tabs) = 0;
    virtual ~Ast() = default;
};

struct String : public Ast {
    std::string value;
    std::string ToString(int tabs) override;
    String(std::string value);
};

struct ColumnName : public Ast {
    std::string value;
    std::string ToString(int tabs) override;
    ColumnName(std::string value);
};

struct Number : public Ast {
    int value;
    std::string ToString(int tabs) override;
    Number(int value);
};

struct Float : public Ast {
    float value;
    std::string ToString(int tabs) override;
    Float(float value);
};

struct Bool : public Ast {
    bool value;
    std::string ToString(int tabs) override;
    Bool(bool value);
};

enum class data_type {
    STR = 1, INT = 2, FLOAT = 3, BOOL = 4,
};
std::string DataTypeStr(data_type dt);

struct Type : public Ast {
    data_type type;
    std::string ToString(int tabs) override;
    Type(int value);
};

struct List : public Ast {
    std::shared_ptr<Ast> list;
    std::shared_ptr<Ast> next;
    List(std::shared_ptr<Ast> element, std::shared_ptr<Ast> next);
    std::string ToString(int tabs) override;
};

struct Pair : public Ast {
    std::shared_ptr<Ast> left;
    std::shared_ptr<Ast> right;
    std::string ToString(int tabs) override;
    Pair(std::shared_ptr<Ast> left, std::shared_ptr<Ast> right);
};

struct Select : public Ast {
    std::string table_name;
    std::shared_ptr<Ast> cmp;
    std::shared_ptr<Ast> join;
    std::string ToString(int tabs) override;
    Select(std::string name, std::shared_ptr<Ast> cmp, std::shared_ptr<Ast> join);
};

struct Delete : public Ast {
    std::string table_name;
    std::shared_ptr<Ast> cmp;
    std::string ToString(int tabs) override;
    Delete(std::string name, std::shared_ptr<Ast> cmp);
};

struct Insert : public Ast {
    std::string table_name;
    std::shared_ptr<Ast> list_values;
    std::string ToString(int tabs) override;
    Insert(std::string name, std::shared_ptr<Ast> values);
};

struct Update : public Ast {
    std::string table_name;
    std::shared_ptr<Ast> cmp;
    std::shared_ptr<Ast> values_list;
    std::string ToString(int tabs) override;
    Update(std::string name, std::shared_ptr<Ast> cmp, std::shared_ptr<Ast> values);
};

struct Create : public Ast {
    std::string table_name;
    std::shared_ptr<Ast> list_values;
    std::string ToString(int tabs) override;
    Create(std::string name, std::shared_ptr<Ast> values);
};

struct Drop : public Ast {
    std::string table_name;
    std::string ToString(int tabs) override;
    Drop(std::string name);
};

enum class logic_op {
    AND = 1, OR = 2,
};
std::string LogicOpStr(logic_op lo);

struct Filter : public Ast {
    logic_op operation;
    std::shared_ptr<Ast> left_filter;
    std::shared_ptr<Ast> right_filter;
    std::string ToString(int tabs) override;
    Filter(int operation_number, std::shared_ptr<Ast> left, std::shared_ptr<Ast> right);
};

enum compare_by {
    GREATER = 1, GREATER_OR_EQUAL = 2, LESS = 3, LESS_OR_EQUAL = 4, EQUAL = 5, NOT_EQUAL = 6, NO_COMPARE = 7,
};
int not_cmp(int cmp);
std::string CompareByStr(compare_by cmp);

struct Compare : public Ast {
    compare_by cmp;
    std::shared_ptr<Ast> left_operand;
    std::shared_ptr<Ast> right_operand;
    std::string ToString(int tabs) override;
    Compare(int cmp_number, std::shared_ptr<Ast> left, std::shared_ptr<Ast> right);
};
```

Чтобы разобрать запрос по токенам используем *Flex* для генерации 
программы, которая будет осуществлять токенизацию

```
%%

"for"       {return FOR;}
"insert"    {return INSERT;}
"update"    {return UPDATE;}
"create"    {return CREATE;}
"remove"    {return REMOVE;}
"drop"      {return DROP;}
"in"        {return IN;}
"filter"    {return FILTER;}
"with"      {return WITH;}
"return"    {return RETURN;}
"join"      {return JOIN;}

"&&"        {yylval.logic_op = 1; return AND;}
"||"        {yylval.logic_op = 2; return OR;}

">"         {yylval.cmp_type = 1; return CMP;}
">="        {yylval.cmp_type = 2; return CMP;}
"<"         {yylval.cmp_type = 3; return CMP;}
"<="        {yylval.cmp_type = 4; return CMP;}
"=="        {yylval.cmp_type = 5; return CMP;}
"!="        {yylval.cmp_type = 6; return CMP;}

"str"       {yylval.type = 1; return TYPE;}
"int"       {yylval.type = 2; return TYPE;}
"float"     {yylval.type = 3; return TYPE;}
"bool"      {yylval.type = 4; return TYPE;}
"true"      {yylval.boolval = 1; return BOOL;}
"false"     {yylval.boolval = 0; return BOOL;}

"("         {return OPEN_CIRCLE_BRACKET;}
")"         {return CLOSE_CIRCLE_BRACKET;}
"{"         {return OPEN_FIGURE_BRACKET;}
"}"         {return CLOSE_FIGURE_BRACKET;}
";"         {return ENDLINE;}
":"         {return COLON;}
"."         {return DOT;}
","         {return COMMA;}
"\""        {return QUOTE;}

{word}     {
    sscanf(yytext, "%s", yylval.str);
    return (STR);
}
{int}   {
    yylval.intval = atoi(yytext);
    return (INT);
}
{float}     {
    yylval.floatval = atof(yytext);
    return (FLOAT);
}

[ \t]   { /* ignore */ }
[\n]    {}
.           {
    print_error(yytext);
    return (OTHER);
}

%%
```

Далее, ввод, разбитый на токены передается парсеру выражения, который разюирает его
и с помощью наших функций и определенных фабрик, формирует Ast

```bison

%%

root:
|   root query ENDLINE { visit_ast($2); printf("$> "); }
;

query: select_query
|   insert_query
|   delete_query
|   drop_query
|   update_query
|   drop_query
|   create_query
;

select_query:
|   empty_select
|   filter_select
|   join_empty_select
|   join_filter_select
;
empty_select:
|   FOR STR IN STR RETURN STR { $$ = new_select($4, nullptr, nullptr); }
;
filter_select:
|   FOR STR IN STR FILTER filter_statement RETURN STR { $$ = new_select($4, $6, nullptr); }
;

join_empty_select:
|   FOR STR IN STR FOR STR IN STR RETURN STR "," STR { $$ = new_select($4, nullptr, new_select($8, nullptr, nullptr)); }
;

join_filter_select:
|   FOR STR IN STR FOR STR IN STR FILTER filter_statement RETURN STR "," STR { $$ = new_select($4, $10, new_select($8, nullptr, nullptr)); }
;


update_query:
|   empty_update
|   filter_update
;
empty_update:
|   FOR STR IN STR UPDATE STR WITH "{" values_list "}" IN STR { $$ = new_update($4, nullptr, $9); }
;
filter_update:
|   FOR STR IN STR FILTER filter_statement UPDATE STR WITH "{" values_list "}" IN STR { $$ = new_update($4, $6, $11); }
;

delete_query:
|   empty_delete
|   filter_delete
;
empty_delete:
|   FOR STR IN STR REMOVE STR IN STR { $$ = new_delete($4, nullptr); }
;
filter_delete:
|   FOR STR IN STR FILTER filter_statement REMOVE STR IN STR { $$ = new_delete($4, $6);}
;

insert_query:
|   INSERT "{" values_list "}" IN STR { $$ = new_insert($6, $3); }
;

create_query:
|   CREATE STR "{" values_list "}" { $$ = new_create($2, $4); }
;

drop_query:
|   DROP STR { $$ = new_drop($2); }
;

values_list:
|   values_list "," pair { $$ = new_list($3, $1); }
|   pair { $$ = new_list($1, nullptr); }
;

pair:
|   column ":" terminal { $$ = new_pair($1, $3); }
;

filter_statement:
|   filter_statement "&&" filter_statement { $$ = new_filter($2, $1, $3); }
|   filter_statement "||" filter_statement { $$ = new_filter($2, $1, $3); }
|   "(" filter_statement ")" { $$ = $2; }
|   logic_statement
;

logic_statement:
|   STR DOT column CMP terminal { $$ = new_compare($4, $3, $5); }
|   terminal CMP STR DOT column { $$ = new_compare(not_cmp($2), $1, $5); }
|   STR DOT column CMP STR DOT column { $$ = new_compare($4, $3, $7); }
;

column:
|   STR { $$ = new_name($1); }
;

terminal:
|   TYPE { $$ = new_type($1); }
|   INT { $$ = new_number($1); }
|   FLOAT { $$ = new_float($1); }
|   BOOL { $$ = new_bool($1); }
|   QUOTE STR QUOTE { $$ = new_string($2); }
;

%%
```

Таким образом мы получим Ast, которое в дальнейшем можно будет выполнить

В результате разбора каждого подвыражения вызывается код фабрик, которые формируют 
указатели на объекты структур

```c++
Ast* new_string(std::string value) {
    return new String(std::move(value));
}
Ast* new_name(std::string value) {
    return new ColumnName(std::move(value));
}
Ast* new_number(int value) {
    return new Number(value);
}
Ast* new_float(float value) {
    return new Float(value);
}
Ast* new_bool(bool value) {
    return new Bool(value);
}
Ast* new_type(int type) {
    return new Type(type);
}
Ast* new_list(Ast* operand, Ast* prev) {
    return new List(std::shared_ptr<Ast>(operand), std::shared_ptr<Ast>(prev));
}
Ast* new_pair(Ast* left, Ast* right) {
    return new Pair(std::shared_ptr<Ast>(left), std::shared_ptr<Ast>(right));
}
Ast* new_select(std::string name, Ast* cmp, Ast* join) {
    return new Select(std::move(name), std::shared_ptr<Ast>(cmp), std::shared_ptr<Ast>(join));
}
Ast* new_delete(std::string name, Ast* cmp) {
    return new Delete(std::move(name), std::shared_ptr<Ast>(cmp));
}
Ast* new_insert(std::string name, Ast* values) {
    return new Insert(std::move(name), std::shared_ptr<Ast>(values));
}
Ast* new_update(std::string name, Ast* cmp, Ast* values) {
    return new Update(std::move(name), std::shared_ptr<Ast>(cmp), std::shared_ptr<Ast>(values));
}
Ast* new_create(std::string name, Ast* values) {
    return new Create(std::move(name), std::shared_ptr<Ast>(values));
}
Ast* new_drop(std::string name) {
    return new Drop(std::move(name));
}
Ast* new_filter(int operation, Ast* left, Ast* right) {
    return new Filter(operation, std::shared_ptr<Ast>(left), std::shared_ptr<Ast>(right));
}
Ast* new_compare(int cmp, Ast* left, Ast* right) {
    return new Compare(cmp, std::shared_ptr<Ast>(left), std::shared_ptr<Ast>(right));
}
```

## Результаты

`select` запрос с одним условием 
```
FOR u IN users FILTER u.status == "active" RETURN u;

Select {
 table: users
 cmp: 
  Compare { 
   ColumnName { status }
   EQUAL
   String { active }
  }
 join: nullptr
}
```

`select` запрос с логической комбинацией условий (как мы видим, операция `AND` более приоритетная)
```
FOR u IN users FILTER u.status == "active" || u.age > 18 && u.age < 20 return u;

Select {
 table: users
 cmp: 
  Filter { 
   Compare { 
    ColumnName { status }
    EQUAL
    String { active }
   }
   OR
   Filter { 
    Compare { 
     ColumnName { age }
     GREATER
     Number { 18 }
    }
    AND
    Compare { 
     ColumnName { age }
     LESS
     Number { 20 }
    }
   }
  }
 join: nullptr
}
```

`cross-join` запрос 
```
FOR u IN users FOR f IN friends RETURN u, f;

Select {
 table: users
 cmp: nullptr
 join: 
  Select {
   table: friends
   cmp: nullptr
   join: nullptr
  }
}
```

`inner-join` запрос с условиями на таблицы
```
FOR u IN users FOR f IN friends FILTER u.id == f.userId && f.active == true && (u.status == "active" || f.age > 15) RETURN u, f;

Select {
 table: users
 cmp: 
  Filter { 
   Filter { 
    Compare { 
     ColumnName { id }
     EQUAL
     ColumnName { userId }
    }
    AND
    Compare { 
     ColumnName { active }
     EQUAL
     Bool { 1 }
    }
   }
   AND
   Filter { 
    Compare { 
     ColumnName { status }
     EQUAL
     String { active }
    }
    OR
    Compare { 
     ColumnName { age }
     GREATER
     Number { 15 }
    }
   }
  }
 join: 
  Select {
   table: friends
   cmp: nullptr
   join: nullptr
  }
}
```

`update` запрос с условием 
```
FOR u IN users FILTER u.status == "active" UPDATE u WITH { status: "inactive" } IN users;

Update {
 table: users
 cmp: 
  Compare { 
   ColumnName { status }
   EQUAL
   String { active }
  }
 list_values:
  List { 
   Pair { 
    ColumnName { status }
    String { inactive }
   }; 
  }
}
```

`delete` запрос
```
FOR u IN users REMOVE u IN users;

Delete {
 table: users
 cmp: nullptr
}
```

`delete` запрос с условием
```
FOR u IN users FILTER u.status == "active" || u.age > 18 && u.age < 20 REMOVE u IN users;

Delete {
 table: users
 cmp: 
  Filter { 
   Compare { 
    ColumnName { status }
    EQUAL
    String { active }
   }
   OR
   Filter { 
    Compare { 
     ColumnName { age }
     GREATER
     Number { 18 }
    }
    AND
    Compare { 
     ColumnName { age }
     LESS
     Number { 20 }
    }
   }
  }
}
```

`insert` запрос
```
INSERT { firstName: "Anna", name: "Pavlova", profession: "artist" } IN users;

Insert {
 name: users
 List { 
  Pair { 
   ColumnName { profession }
   String { artist }
  }; 
  Pair { 
   ColumnName { name }
   String { Pavlova }
  }; 
  Pair { 
   ColumnName { firstName }
   String { Anna }
  }; 
 }
}
```

`create` запрос
```
CREATE users { ID: INT, active: STR, else: BOOL };

Create {
 name: users
 List { 
  Pair { 
   ColumnName { else }
   Type { BOOL }
  }; 
  Pair { 
   ColumnName { active }
   Type { STR }
  }; 
  Pair { 
   ColumnName { ID }
   Type { INT }
  }; 
 }
}
```

## Вывод

В ходе выполнения лабораторной работы я научился работать с *Flex* и *Bison*,
А также формировать Ast из разобранного выражения
