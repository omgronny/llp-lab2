#include "ast.h"

static const std::string tab = " ";

std::string String::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "String { " + value + " }";
}

std::string ColumnName::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "ColumnName { " + value + " }";
}

std::string Number::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "Number { " + std::to_string(value) + " }";
}


std::string Float::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "Float { " + std::to_string(value) + " }";
}


std::string Bool::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "Bool { " + std::to_string(value) + " }";
}


std::string Type::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "Type { " + DataTypeStr(type) + " }";
}


std::string List::ToString(int tabs) {
    std::string res;
    std::cout << "list" << std::endl;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "List { \n";
    auto item = shared_from_this();
    while (item != nullptr) {
        res += As<List>(item.get())->list->ToString(tabs + 1) + "; \n";
        item = As<List>(item.get())->next;
    }
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "}";
}

std::string Pair::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "Pair { \n" + left->ToString(tabs + 1) + "\n";
    res += right->ToString(tabs + 1) + "\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "}";
}


std::string Select::ToString(int tabs) {

    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += " ";
    }
    res += "Select {\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "table: " + table_name + "\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "cmp: ";

    if (cmp != nullptr) {
        res += "\n";
        res += cmp->ToString(tabs + 2);
    } else {
        res += "nullptr";
    }
    res += "\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "join: ";
    if (join != nullptr) {
        res += "\n";
        res += join->ToString(tabs + 2);
    } else {
        res += "nullptr";
    }
    res += "\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "}";
}


std::string Delete::ToString(int tabs) {

    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "Delete {\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "table: " + table_name + "\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "cmp: ";

    if (cmp != nullptr) {
        res += "\n";
        res += cmp->ToString(tabs + 2);
    } else {
        res += "nullptr";
    }
    res += "\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "}";
}

std::string Insert::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "Insert {\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "name: " + table_name + "\n" + list_values->ToString(tabs + 1) + "\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "}";
}


std::string Update::ToString(int tabs) {

    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "Update {\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "table: " + table_name + "\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "cmp: ";

    if (cmp != nullptr) {
        res += "\n";
        res += cmp->ToString(tabs + 2);
    } else {
        res += "nullptr";
    }
    res += "\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "list_values:\n" + values_list->ToString(tabs + 2) + "\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "}";
}

std::string Create::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "Create {\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += "name: " + table_name + "\n" + list_values->ToString(tabs + 1) + "\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "}";
}

std::string Drop::ToString(int tabs) {

    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "Drop { " + table_name + " }";

    return res;
}

std::string Filter::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "Filter { \n" + left_filter->ToString(tabs + 1) + "\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += LogicOpStr(operation) + "\n" + right_filter->ToString(tabs + 1) + "\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "}";
}


std::string Compare::ToString(int tabs) {
    std::string res;
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    res += "Compare { \n" + left_operand->ToString(tabs + 1) + "\n";
    for (int i = 0; i < tabs + 1; ++i) {
        res += tab;
    }
    res += CompareByStr(cmp) + "\n" + right_operand->ToString(tabs + 1) + "\n";
    for (int i = 0; i < tabs; ++i) {
        res += tab;
    }
    return res + "}";
}


