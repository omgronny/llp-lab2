all: ast.h ast.cpp parser.y tokenizer.l
	make clean
	make aql
	make run

aql:
	bison -d parser.y -Wcounterexamples
	flex tokenizer.l
	g++ -g -o $@ parser.tab.c lex.yy.c ast.h ast.cpp to_string.cpp visitor.h

run:
	./aql

clean:
	rm -f aql lex.yy.c parser.tab.c parser.tab.h

